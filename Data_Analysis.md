<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DS0105EN-SkillsNetwork/labs/Module2/images/SN_web_lightmode.png">


## Hands-on Lab: Treating Duplicate Data 

<b>Estimated time: 30 min </b>

In this lab, you will get overview of how to treat duplicate data and ottliers present in the dataset and how to treat them using Python.

## Objectives

After completing this lab, you will be able to:

1.  Handle duplicate values 

## What is Duplicate Data 

<b>Duplicate data</b> is any record that unintentionally shares data with another record in a Dataset. Duplicate data is easy to spot and mainly occurs when transferring data between systems. 

Duplicate records make your data dirty. Any reports generated from such data will not be accurate. Hence, we cannot rely on them to make wise decisions. So before any analysis on data we need to remove duplicate data for dataset.


 ### Installing required Modules

As said above we will be learning using Python Pandas modules. To install the required modules:

> Open Jupyter notebook in SN lab environment.

> Copy the below code and paste in the notebook code cell and run the code

 ```
   pip install pandas
   ```
---

### Importing the Dataset

When we are using pandas, we required the data frames. Let us first see the way to load the data frame from the file.

> To load the data csv file, copy the below code and paste it in code cell and run the code.

```
 import pandas as pd
```

```
df=pd.read_excel('EDA Cars.xlsx')
```
lets have a look on data:
```
df.head()
```
![image](./Images/Screenshot1.png)

The Dataset consist of following colums:

- **INCOME**: Income of the user.
- **MARITAL STATUS**: Marital Status of the user.
- **SEX**: Gender of User.
- **EDUCATION**: Education qualification of the user.
- **JOB**: Job type of the user.
- **TRAVEL TIME**: Travel Time of the user.
- **USE**: Use of the vehicle.
- **MILES CLOCKED**: Total miles traveled by vehicle.
- **Name**: First name of the user.
- **CAR TYPE**: Type of car.
- **CAR AGE**: Age of car.
- **CITY**: Curret city of the user.
- **POSTAL CODE**: Curret Postal code of the user.

Lets check the sape of data

```
df.shape
```
> Output: 
>   (303, 13)

Shape attribute tells us number of observations and variables we have in the data set. It is used to check the dimension of data. The cars data set has 303 observations and 13 variables in the data set.

<b>Now let us check the duplicate values present in data using duplicated() function.</b>

Copy the below code and paste it in code cell and run the code.

   ```
   dups = df.duplicated()
   print('Number of duplicate rows = %d' % (dups.sum()))

   df[dups]
   ```

Output
![image](./Images/Screenshot2.png)

Since we have 14 duplicate records in the data, we will remove this from the data set so that we get only distinct records.

>Now let us drop the duplicate values present in data using drop_duplicates() function.

Copy the below code and paste it in code cell and run the code.

````
df.drop_duplicates(inplace=True) 
````
Post removing the duplicate, we will check whether the duplicates has been removed from the data set or not.

```
dups = df.duplicated()
print('Number of duplicate rows = %d' % (dups.sum()))

df[dups]
```
Output
![image](./Images/Screenshot3.png)

Now, we can clearly see that there are no duplicate records in the data set. We can also quickly confirm the number of records by using the shape attribute as those 14 records should be removed from the original data. Initially it had 303 records now it should have 289.

```
df.shape
```
> Output: 
> (289, 13)


# Exercise : Practice Task

You will practice some tasks on the data.

1.  > Write the code to prints information about the DataFrame..
    <details>
     <summary>Answer</summary>
     
    > df.info()

       Output

      ![image](./Images/Screenshot4.png)

    </details>

2.  > Write the code to Convert Postel Code into Category ..
    <details>
     <summary>Answer</summary>
     
    > df["POSTAL CODE"]= pd.Categorical(df['POSTAL CODE'])

    > df.info()

       Output
       
      ![image](./Images/Screenshot5.png)

    </details>

## Authors

[Pooja Patel](https://www.linkedin.com/in/pooja-patel-965238216/)

## Reference link

[Markdown Cheatsheet 1](https://www.markdown-cheatsheet.com/)

[Markdown Cheatsheet 2](https://www.geeksforgeeks.org/what-is-readme-md-file/)

## Change Log

| Date (YYYY-MM-DD) | Version | Changed By      | Change Description      |
| ----------------- | ------- | -------------   | ----------------------- |
| 2022-12-06        | 1.0     |   Pooja Patel | Created initial version |
